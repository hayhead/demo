import com.ServerApplication;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.mapper.BookMapper;
import com.pojo.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServerApplication.class)
public class MyTest {

    @Autowired
    BookMapper bookMapper;
    @Test
    public void test(){
        List<Book> books = bookMapper.findBooks();
        System.out.println(Arrays.asList(books));
    }
}
