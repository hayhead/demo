package com.mapper;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.pojo.Book;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface BookMapper {

    @Select("select * from books")
    public List<Book> findBooks();

    @Insert("insert into books values(#{isbn},#{bookName},#{author})")
    public int addBook(Book book);

    @Update("update books set book_name=#{bookName},author=#{author} where isbn=#{isbn}")
    public int updateBook(Book book);

    @Delete("delete from books where isbn=#{isbn}")
    public int deleteBook(String isbn);

    @Select("select * from books where book_name=#{bookName}")
    public List<Book> findBookByBookName(String bookName);

    @Select("select * from books where author=#{author}")
    public List<Book> findBookByAuthor(String author);
}
